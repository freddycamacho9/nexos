package com.springboot.app.controller;

import static java.util.Objects.isNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.tomcat.util.json.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsObject.Options;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.mongodb.util.JSON;
import com.springboot.app.models.dao.INucleoDao;
import com.springboot.app.models.entity.Auditoria;
import com.springboot.app.models.entity.DatabaseSequence;
import com.springboot.app.models.entity.Nucleo;
import com.springboot.app.models.entity.Persona;
import com.springboot.app.models.entity.PersonaRequest;
import com.springboot.app.models.entity.PersonaResponse;
import com.springboot.app.models.service.IAuditoriaService;
import com.springboot.app.models.service.INucleoService;
import com.springboot.app.models.service.IPersonaService;
import com.springboot.app.models.service.ISequenceService;

import lombok.val;
import lombok.extern.log4j.Log4j2;


@RestController
@RequestMapping("/nexus")
@Log4j2
public class PersonaController {
	
	@Autowired
	private IPersonaService personaService;
	
	@Autowired
	private INucleoService nucleoService;
	
	@Autowired
	private IAuditoriaService audService;
	
	@Autowired
	private ISequenceService nextSequenceService;
	
	Auditoria au = new Auditoria();
	
	@PostMapping("/administrarPersona")
	public ResponseEntity<?> crearPersona(@RequestBody PersonaRequest persona) {
		
		
		//Logs de petición y almacenamiento en bd
		String JSON = new Gson().toJson(persona);
		log.info("Se realiza petición POST: "+ JSON);
		au.setFecha(new Date());
		au.setMensaje("Se realiza petición POST: "+ JSON);
		audService.save(au);
		//Se realiza validación de campos
		val validationErrors = validate(persona);
		
		if (validationErrors.isPresent()) {
			log.info(validationErrors.get());
			au.setMensaje(validationErrors.get());
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje(validationErrors.get()), HttpStatus.BAD_REQUEST);
		}
		try {
			
		//Se crea el objeto persona para llenar los datos y guardar en la base de datos
		Persona p = new Persona();
			
		p.setNombre(persona.getCrearPersona().getNombre());
		p.setApellido(persona.getCrearPersona().getApellido());
		p.setCedula(persona.getCrearPersona().getDocumento());
		p.setDireccion(persona.getCrearPersona().getDireccion());
		p.setDocumento(persona.getCrearPersona().getTipoDocumento());
		p.setParentesco(persona.getCrearPersona().getParentesco());
		p.setTelefono(persona.getCrearPersona().getTelefono());
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date fecha=null;
		fecha = simpleDateFormat.parse(persona.getCrearPersona().getFechaNacimiento());
		
		p.setFecha_nac(fecha);
		//Agregar nucleo familiar
		p.setNucleo(persona.getCrearPersona().getNucleo());	
		
		//Validaciones
		
		//1. Si el campo eliminar está en "Yes" y se encuentra el registro, eliminará el usuario
		
		Persona buscarPersona = personaService.findbyDocument(p.getCedula());
		
		if(buscarPersona!=null && persona.getCrearPersona().getEliminar().equals("Yes")) {
			personaService.delete(buscarPersona.getId());
			nucleoService.delete(buscarPersona.getId());
			log.info("Persona eliminada con éxito");
			au.setMensaje("Persona eliminada con éxito" + " ID: "+buscarPersona.getId());
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje("Persona eliminada con éxito"), HttpStatus.OK);
		}
		
		//2. Si ID existe, entonces actualizará el registro
		
		if(buscarPersona!=null) {
			p.setId(buscarPersona.getId());
			personaService.save(p);
			
			Nucleo n = new Nucleo();
			n.setId(p.getId());
			n.set_persona(p);
			n.setNucleo(persona.getCrearPersona().getNucleo());
			
			nucleoService.save(n);
			log.info("Persona actualizada con éxito");
			au.setMensaje("Persona actualizada con éxito" + " ID: "+buscarPersona.getId());
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje("Persona actualizada con éxito"), HttpStatus.OK);
		}
		//Sino entonces ingresará el registro
		else {
			//ID autoincremental
			p.setId(nextSequenceService.getNextSequence(Persona.SEQUENCE_NAME));
			
			personaService.save(p);
			
			Nucleo n = new Nucleo();
			n.setId(p.getId());
			n.set_persona(p);
			n.setNucleo(persona.getCrearPersona().getNucleo());
			
			nucleoService.save(n);
			log.info("Persona guardada con éxito");
			au.setMensaje("Persona guardada con éxito" + " ID: "+p.getId());
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje("Persona guardada con éxito"), HttpStatus.OK);
		}
		}catch (Exception e) {
			log.info(e.getMessage());
			au.setMensaje(e.getMessage());
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje("Ha ocurrido un error, por favor verifique logs"),
					HttpStatus.BAD_REQUEST);
		}
			
	}
	
	@GetMapping("/administrarPersona")
	public ResponseEntity<?> consultarPersona(@RequestParam(name="documento") String documento) {
		
		log.info("Búsqueda por documento: "+ documento);
		au.setMensaje("Búsqueda por documento: "+ documento);
		audService.save(au);
		//Se realiza búsqueda de persona por documento.
		
		try {
			
		Persona buscarPersona = personaService.findbyDocument(documento);
		
		if(buscarPersona!=null) {
			//Se crea objeto para llenar con la información de salida
			PersonaResponse.Busqueda pr = new PersonaResponse.Busqueda();
			
			pr.setNombre(buscarPersona.getNombre());
			pr.setApellido(buscarPersona.getApellido());
			pr.setDireccion(buscarPersona.getDireccion());
			pr.setDocumento(buscarPersona.getCedula());
			pr.setParentesco(buscarPersona.getParentesco());
			pr.setTipoDocumento(buscarPersona.getDocumento());
			pr.setTelefono(buscarPersona.getTelefono());
			
			//calcular la edad actual
						
			pr.setEdad(calculaEdad(buscarPersona.getFecha_nac()).toString()+" años");
			
			//Se busca información acerca del núcleo familiar
			
			List<Persona> listPer = new ArrayList<Persona>();
			List<Nucleo> listNuc = new ArrayList<Nucleo>();
			
			listNuc = nucleoService.findByNucleo(buscarPersona.getNucleo());
			
			if(listNuc!=null) {
				for(Nucleo n : listNuc) {
					if(n.get_persona().getId()!=buscarPersona.getId()) {
						
						listPer.add(n.get_persona());
					}
				}
			}
			
			pr.setNucleo(listPer);
			String JSON = new Gson().toJson(pr);
			log.info("Response: "+JSON);
			au.setMensaje("Response: "+JSON);
			audService.save(au);
			return new ResponseEntity<>(pr, HttpStatus.OK);
			
		}else {
			//Sino se encuentra información, el response será un 404
			log.info("Persona no encontrada");
			au.setMensaje("Persona no encontrada");
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje("Persona no encontrada"), HttpStatus.NOT_FOUND);
		}
		}catch(Exception e) {
			log.info(e.getMessage());
			au.setMensaje(e.getMessage());
			audService.save(au);
			return new ResponseEntity<>(new PersonaResponse.Mensaje("Ha ocurrido un error, por favor verifique logs"),
					HttpStatus.BAD_REQUEST);
		}
	}
	//Método que realiza validaciones de campos
	private static Optional<String> validate(PersonaRequest p) {
		val errors = new StringBuilder();
		val req = p.getCrearPersona();

		if (isNull(req)) {
			errors.append("CrearPersonaRequestMessage es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getNombre())) {
			errors.append("El campo nombre es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getApellido())) {
			errors.append("El campo apellido es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getDocumento())) {
			errors.append("El campo documento es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getTipoDocumento())) {
			errors.append("El campo tipoDocumento es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getTelefono())) {
			errors.append("El campo teléfono es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getFechaNacimiento())) {
			errors.append("El campo fechaNacimiento es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getDireccion())) {
			errors.append("El campo dirección es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getParentesco())) {
			errors.append("El campo parentesco es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getEliminar())) {
			errors.append("El campo eliminar es requerido");
			return Optional.of(errors.toString());
		}
		if (isNull(req.getNucleo())) {
			errors.append("El campo nucleo es requerido");
			return Optional.of(errors.toString());
		}
		if (req.getEliminar().equals("Yes") || req.getEliminar().equals("No")) {
			return Optional.empty();
		} else {
			errors.append("El campo eliminar debe tener los valores 'Yes' o 'No'");
			return Optional.of(errors.toString());
		}

	}
	//Método para calcular la edad actual
	private Long calculaEdad(Date fecha_nac) {
		
		       Calendar fechaNacimiento = Calendar.getInstance();
		       //Se crea un objeto con la fecha actual
		       Calendar fechaActual = Calendar.getInstance();
		       //Se asigna la fecha recibida a la fecha de nacimiento.
		       fechaNacimiento.setTime(fecha_nac);
		       //Se restan la fecha actual y la fecha de nacimiento
		       int año = fechaActual.get(Calendar.YEAR)- fechaNacimiento.get(Calendar.YEAR);
		       int mes =fechaActual.get(Calendar.MONTH)- fechaNacimiento.get(Calendar.MONTH);
		       int dia = fechaActual.get(Calendar.DATE)- fechaNacimiento.get(Calendar.DATE);
		       //Se ajusta el año dependiendo el mes y el día
		       if(mes<0 || (mes==0 && dia<0)){
		           año--;
		       }
		       //Regresa la edad en base a la fecha de nacimiento
		       return (long) año;
		   
		}

}
	