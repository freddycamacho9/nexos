package com.springboot.app.models.service;

public interface ISequenceService {

	
	public long getNextSequence(String seqName);
}
