package com.springboot.app.models.service;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.app.models.dao.IPersonaDao;
import com.springboot.app.models.entity.Persona;




@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	private IPersonaDao personaDao;
	
	@Autowired
	private MongoOperations mongo;
	
	@Override
	@Transactional
	public void save(Persona persona) {
		// TODO Auto-generated method stub
		personaDao.save(persona);
	}
	
	@Override
	@Transactional
	public Persona findOne(Long id) {
		return personaDao.findById(id).orElse(null); 
	}
	
	@Override
	@Transactional
	public void delete (Long id) {
		personaDao.deleteById(id);
	}
	
	@Override
	@Transactional
	public Persona findbyDocument(String document) {
		Persona p = mongo.findOne(new Query(Criteria.where("cedula").is(document)), Persona.class);
		return !Objects.isNull(p) ? p : null;
	}
	
	@Override
	@Transactional
	public List<Persona> findByNucleo(Long nucleo){
		List<Persona> p = mongo.find(new Query(Criteria.where("nucleo").is(nucleo)), Persona.class);
		return p!=null ? p : null;
	}

}
