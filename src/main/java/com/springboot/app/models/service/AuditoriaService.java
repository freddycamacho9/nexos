package com.springboot.app.models.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.app.models.dao.IAuditoriaDao;
import com.springboot.app.models.entity.Auditoria;

@Service
public class AuditoriaService implements IAuditoriaService {
	
	@Autowired
	private IAuditoriaDao auditoriaDao;

	@Override
	public void save(Auditoria aud) {
		// TODO Auto-generated method stub
		auditoriaDao.save(aud);
	}

}
