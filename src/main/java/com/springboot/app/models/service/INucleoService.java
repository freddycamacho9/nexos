package com.springboot.app.models.service;

import java.util.List;

import com.springboot.app.models.entity.Nucleo;

public interface INucleoService {
	
	public void save(Nucleo nucleo);
	
	public void delete (Long id);
	
	public List<Nucleo> findByNucleo(Long nucleo);

}
