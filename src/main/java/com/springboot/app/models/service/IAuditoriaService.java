package com.springboot.app.models.service;

import com.springboot.app.models.entity.Auditoria;

public interface IAuditoriaService {
	
	public void save(Auditoria aud);

}
