package com.springboot.app.models.service;

import java.util.List;

import com.springboot.app.models.entity.Persona;

public interface IPersonaService {

	public void save(Persona persona);
	
	public Persona findOne(Long id);
	
	public void delete (Long id);
	
	public Persona findbyDocument(String document);
	
	public List<Persona> findByNucleo(Long nucleo);
}
