package com.springboot.app.models.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.gridfs.GridFsObject.Options;
import org.springframework.stereotype.Service;

import com.springboot.app.models.entity.DatabaseSequence;

@Service
public class SequenceService implements ISequenceService{
	
	@Autowired
	private MongoOperations mongo;

    public long getNextSequence(String seqName)
    {
        DatabaseSequence c = mongo.findAndModify(new Query(Criteria.where("_id").is(seqName)), new Update().inc("seq", 1), DatabaseSequence.class);
        
        return !Objects.isNull(c) ? c.getSeq() : 1;
    }

}
