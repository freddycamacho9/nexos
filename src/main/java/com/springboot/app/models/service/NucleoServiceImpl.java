package com.springboot.app.models.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.springboot.app.models.dao.INucleoDao;
import com.springboot.app.models.entity.Nucleo;
import com.springboot.app.models.entity.Persona;

@Service
public class NucleoServiceImpl implements INucleoService{
	
	@Autowired
	private INucleoDao nucleoDao;
	
	@Autowired
	private MongoOperations mongo;

	@Override
	@Transactional
	public void save(Nucleo nucleo) {
		// TODO Auto-generated method stub
		nucleoDao.save(nucleo);
	}
	
	@Override
	@Transactional
	public List<Nucleo> findByNucleo(Long nucleo){
		List<Nucleo> p = mongo.find(new Query(Criteria.where("nucleo").is(nucleo)), Nucleo.class);
		return p!=null ? p : null;
	}

	@Override
	@Transactional
	public void delete(Long id) {
		// TODO Auto-generated method stub
		nucleoDao.deleteById(id);
	}

}
