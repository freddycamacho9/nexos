package com.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.app.models.entity.Auditoria;

public interface IAuditoriaDao extends CrudRepository<Auditoria, Long>{

}
