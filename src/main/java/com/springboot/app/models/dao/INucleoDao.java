package com.springboot.app.models.dao;

import org.springframework.data.repository.CrudRepository;

import com.springboot.app.models.entity.Nucleo;

public interface INucleoDao extends CrudRepository<Nucleo, Long>{

}
