package com.springboot.app.models.dao;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.springboot.app.models.entity.Persona;;

public interface IPersonaDao extends CrudRepository<Persona, Long>{
	
}
