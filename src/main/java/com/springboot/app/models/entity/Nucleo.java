package com.springboot.app.models.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "nucleo")
public class Nucleo {
	
	@Id
	private Long id;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	private Long nucleo;
	
	private Persona persona;

	public Long getNucleo() {
		return nucleo;
	}

	public void setNucleo(Long nucleo) {
		this.nucleo = nucleo;
	}

	public Persona get_persona() {
		return persona;
	}

	public void set_persona(Persona personas) {
		this.persona = personas;
	}

}
