package com.springboot.app.models.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.springboot.app.models.entity.PersonaRequest;
import com.springboot.app.models.entity.PersonaRequest.PersonaRequestBuilder;
import com.springboot.app.models.entity.PersonaRequest.CrearPersona;
import com.springboot.app.models.entity.PersonaRequest.CrearPersona.CrearPersonaBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonaRequest {
	
	@JsonProperty("CrearPersonaRequestMessage")
    private CrearPersona crearPersona;
	
	@Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CrearPersona {
	    private String nombre;
	    private String apellido;
	    private String tipoDocumento;
	    private String documento;
	    private String telefono;
	    private String fechaNacimiento;
	    private String direccion;
	    private String parentesco;
	    private String eliminar;
	    private Long nucleo;
    }
	
	

}
