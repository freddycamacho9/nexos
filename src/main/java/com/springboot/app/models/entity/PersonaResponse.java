package com.springboot.app.models.entity;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.springboot.app.models.entity.Persona;
import com.springboot.app.models.entity.PersonaResponse;
import com.springboot.app.models.entity.PersonaResponse.Mensaje;
import com.springboot.app.models.entity.PersonaResponse.Mensaje.MensajeBuilder;
import com.springboot.app.models.entity.PersonaResponse.PersonaResponseBuilder;
import com.springboot.app.models.entity.Persona;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PersonaResponse {
	
	private Mensaje message;
	
	@JsonProperty("ConsultaPersonaResponseMessage")
	private Busqueda busqueda;
	
	@Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Mensaje {
	 @JsonProperty("PersonaResponseMessage")
	  private String message;
    }
	
	 @Data
	    @Builder
	    @AllArgsConstructor
	    @NoArgsConstructor
	    public static class Busqueda {
		    private String nombre;
		    private String apellido;
		    private String edad;
		    private String tipoDocumento;
		    private String documento;
		    private String direccion;
		    private String telefono;
		    private String parentesco;
		    private List<Persona> nucleo;
	    }

}
