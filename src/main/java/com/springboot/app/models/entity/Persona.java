package com.springboot.app.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@Document(collection = "people")
@JsonPropertyOrder({"id", "documento", "cedula", "nombre", "apellido", "direccion", "telefono", "parentesco"})
public class Persona implements Serializable{

	private static final long serialVersionUID = 1L;

	@Transient
	public static final String SEQUENCE_NAME = "person_sequence";
	
	@Id
	@NonNull
	//Parámetro para identificar el dato almacenado en mongo
	private Long id;
	// Parámetro para almacenar el nombre de la persona
	private String nombre;
	
	//Parámetro para almacenar el apellido de la persona
	private String apellido;
	
	//Parámetro para almacenar el tipo de documento de la persona
	private String documento;
	
	//Parámetro para almacenar el número de cédula de la persona
	private String cedula;
	
	//Parámetro para almacenar la dirección de residencia de la persona
	private String direccion;
	
	//Parámetro para almacenar el número de teléfono de la persona
	private String telefono;
	
	//Parámetro para almacenar el grado de consanguinidad de la persona
	private String parentesco;
	
	//Parámetro para almacenar la fecha de nacimiento de la persona
	private Date fecha_nac;
		
	private Long nucleo;
	
	public Long getNucleo() {
		return nucleo;
	}
	public void setNucleo(Long nucleo) {
		this.nucleo = nucleo;
	}
	public Date getFecha_nac() {
		return fecha_nac;
	}
	public void setFecha_nac(Date fecha_nac) {
		this.fecha_nac = fecha_nac;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getCedula() {
		return cedula;
	}
	public void setCedula(String cedula) {
		this.cedula = cedula;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getParentesco() {
		return parentesco;
	}
	public void setParentesco(String parentesco) {
		this.parentesco = parentesco;
	}

}
